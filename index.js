const { printRequestDetails } = require('./util/printUtils.js')

const express = require('express')
const { v4: uuidv4 } = require('uuid')
const path = require('path')


const app = express()

app.use(express.urlencoded({ extended: true }))


const port = 3000
const host = "0.0.0.0"


function setResponseHeaders(response) {
  response.append("Access-Control-Allow-Origin", "http://webapps.ubuntuserver.com")
}


function handleRootPath(request, response) {
  let requestID = uuidv4();

  printRequestDetails(requestID, request)

  setResponseHeaders(response)

  response.sendFile(path.join(__dirname, '/index.html'))
}


app.get('/csrf', (req, res) => {
  handleRootPath(req, res)
});


app.listen(port, host, () => {
  console.log(`Example app listening at http://${host}:${port}`)
});
